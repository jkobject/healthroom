	<?php
session_start();
if(!$_SESSION['connection']){ // on ammene sur la page listpage /// on explique plus bas le fonctionnement
	     $host = $_SERVER['HTTP_HOST'];
         $uri = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
         $extra = 'accueil.php';
         header("Location: http://$host$uri/$extra");
         exit;
    }
    if($_SESSION['grade']==1){
	     $host = $_SERVER['HTTP_HOST'];
         $uri = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
         $extra = 'infopage.php?number='.$_SESSION['number'];
         header("Location: http://$host$uri/$extra");
         exit;
         }
include("database.php");
?>



	<!DOCTYPE>
	<html>

	<head> 
		<title>HealthRoom</title> 
		<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
		<link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-pink.min.css">

		<script type="text/javascript" src="node_modules/moment/min/moment.min.js"></script>
		<script type="text/javascript" src="mdl-datepicker-/dist/material-datepicker.min.js"></script>  
		<script type="text/javascript" src="Alljs.js"></script>


		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<meta http-equiv="content-type" content="text/html;charset=utf-8" />

		<link rel="stylesheet" type="text/css" href="stylesheet.css"/>
		<script type="text/javascript" src="../Js/VerifForm.js"></script>
	</head>

	<body class="list">
		<div class="header">

			
				<!-- Textfield with Floating Label -->

				<div class="navbar"> <img src="images/logoHelthRoom.png" class="logo" height="40px" onclick="location.href='disconnect.php' ">
				</div>

				<!-- Expandable Textfield -->
				<form method: "get" action="search.php">
				<div class="navbar mdl-textfield mdl-js-textfield mdl-textfield--expandable is-focused">
					<label class="mdl-button mdl-js-button mdl-button--icon" for="search6">
						<i class="material-icons">search</i>
					</label>
					<div class="mdl-textfield__expandable-holder">
						<input class="mdl-textfield__input" type="text" id="search6">
						<label class="mdl-textfield__label" for="sample-expandable">Search...</label>
					</div>
				</div>
				</form>
				<div class="navbar">
					<i class="material-icons menu" onclick="location.href='listpage.php' ">menu</i>
				</div>
				<!----

				---->
				<h5 class="navbar namenav" onclick="location.href='infopage.php?type=1&number=<?php echo $_SESSION['number']; ?>' "> 
					<?php echo $_SESSION['name']; ?> &nbsp; &nbsp;&nbsp; <?php echo $_SESSION['lastname']; ?>
				</h5>
			</div>

		 <div class="datepicker" style="display:none" id="datepicker">
				
				<img src="images/logoHelthRoom.png" height="100px">
				
				<form>

				<div class="inline">
				<div class="space">
					<h5 class="navbar namenav"> 
					FROM :
				</h5>
				<!----
					ICI DETERMINER LES DATES MINIMUM PAR RAPPORT A LA DATE DU SYSTEME

				---->

					<input type="date" name="date" min="<?php
				$today = getdate();
				//echo date

				?>">
					<input type="time" name="time">
				</div>
				<div class="space">
					<h5 class="navbar namenav"> 
					TO :
				</h5>
				
					<input type="date" name="date" min="2017-04-10">
					<input type="time" name="time">
				</div>
				</div>
					&nbsp;&nbsp;&nbsp;
					<!----
				
					CODER SEND TIME AND DATE
					

				---->
					<button onclick="sendTimeAndDate()" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--primary">
  					Book
					</button>
			
			</form>
		
					
						

					</div>

	
		<h2> Medical List </h2>
			<div>
			<table class="list">
				<tr>
					<th> <h3> Doctor </h3></th>
					<th> <h3> Patient </h3></th>
					<th> <h3> Nurse </h3></th>
					<th> <h3> Room </h3></th>
					<th> <h3> Material </h3></th>
				</tr>
				
				<!----
						
						Ici AJouter par rapport à la quantité Les différentes cartes avec un "for"  
						Un onclick qui les colories en violet et qui change leur value par rapport à cela. 


				---->
				<?php 
					$users = Database::searchUserAll(); // on prend tout les users
					$idoc=0;
						$ipat=0;
						$inur=0;
						//on utilise ces valeures après 

					foreach( $users as $user ){ // on les parcours tous un par un 
						
						// on fait correspondre les chiffres aux mots
						if ($user['iddepartement'] ==0) $departement= "aucun";
       						if ($user['iddepartement'] ==1) $departement= "Pédiatrie";
       						if ($user['iddepartement'] ==2) $departement= "Urgences";
       						if ($user['iddepartement'] ==3) $departement= "Oncologie";
       						if ($user['iddepartement'] ==4) $departement= "Chirurgie";
       						if ($user['iddepartement'] ==5) $departement= "Obstétrique";


						if($user['grade']==3){ // on différencies chacun des utilisateur par son poste
							$idoc++;
							//on va instancier un tableau diplay qui va contenir les 5 types de colones en valeure X et toutes les instances de ces types en valeures Y 
							// celles que l'on instancies avec les valeures plus haut justement
							$disp['doc'][$idoc]="<tr><th > <!-- Event card -->";
						$id= $user['iddepartement'].$user['num'];
				$disp['doc'][$idoc].='<div class="demo-card--media mdl-card mdl-shadow--2dp" id="$id" onclick=\"location.href="infopage.php?type=1&number=$user["num"]"\">';
						$disp['doc'][$idoc].='<div class="mdl-card__title mdl-card--expand">';
								$disp['doc'][$idoc].="<h4> Dr.";
									$disp['doc'][$idoc].=$user['lastname'].":<br>";
									$disp['doc'][$idoc].=$departement."<br>";
									$disp['doc'][$idoc].=$user['num']."#";
								$disp['doc'][$idoc].="</h4>";
							$disp['doc'][$idoc].="</div>";
							$disp['doc'][$idoc].="<div class='mdl-card__actions mdl-card--border' onclick='changeColorID($id)'>";
								$disp['doc'][$idoc].="<a class='mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect'>";
								$disp['doc'][$idoc].="	Add to Calendar";
								$disp['doc'][$idoc].="</a></div></th>";
						}
						// il faut bien penser a utiliser les ' ' et les "" pour pas en avoir qui cassent la string genre "herddde"deded"de" 
	

					// le but est bien de tout afficher en faisant un par un chacuns des trucs vu que l'on se dépace en ligne.
							
					
						if($user['grade']==1){
							$ipat++;
			
							$disp['doc'][$ipat]="<th > <!-- Event card -->";
						$id= $user['iddepartement'].$user['num'];
						$disp['doc'][$ipat].='<div class="demo-card--media mdl-card mdl-shadow--2dp" id="$id" onclick="location.href="infopage.php?type=1&number=$user["num"]"">';
							$disp['doc'][$ipat].='<div class="mdl-card__title mdl-card--expand">';
									$disp['pat'][$ipat].="<h4> ".$user['name'];
									$disp['pat'][$ipat]="   ".$user['lastname'].":<br>";
									$disp['pat'][$ipat].=$departement."<br>";
									$disp['pat'][$ipat].=$user['num']."#";
								$disp['pat'][$ipat].="</h4>".
							$disp['pat'][$ipat].="</div>";
							$disp['pat'][$ipat].="<div class='mdl-card__actions mdl-card--border' onclick='changeColorID($id)'>";
								$disp['pat'][$ipat].='<a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">';
								$disp['pat'][$ipat].="	Add to Calendar";
								$disp['pat'][$ipat].="</a></div></th>";
								
						}
						if($user['grade']==2){
						
							$disp['nur'][$inur]="<th > <!-- Event card -->";
							$id= $user['iddepartement'].$user['num'];
						$disp['nur'][$inur].='<div class="demo-card--media mdl-card mdl-shadow--2dp" id="$id" onclick="location.href="infopage.php?type=1&number=$user["num"]"">';
							$disp['nur'][$inur].="<div class='mdl-card__title mdl-card--expand' >";
								$disp['nur'][$inur].="<h4> ".$user['name'];
									$disp['nur'][$inur].="   ".$user['lastname'].":<br>";
									$disp['nur'][$inur].=$departement."<br>";
									$disp['nur'][$inur].=$user['num']."#";
								$disp['nur'][$inur].="</h4>";
							$disp['nur'][$inur].="</div>";
							$disp['nur'][$inur].='<div class="mdl-card__actions mdl-card--border" onclick="changeColorID($id)">';
								$disp['nur'][$inur].='<a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">';
								$disp['nur'][$inur].="	Add to Calendar";
								$disp['nur'][$inur].="</a></div></th>";
								
						}

					}

					/****** 

					Pédiatrie

					***/

					for($i=1;$i<5;$i++){ // on veut afficher les rooms(#2) et les salles d'opérations (#1)
						$ide=$i+1;
						$id='2'.$ide;
						$disp['room'][$ide].='<th><div class="demo-card--media mdl-card mdl-shadow--2dp" id="$ide" onclick="location.href="infopage.php?type=2&number=$ide"">';
							$disp['room'][$ide].='<div class="mdl-card__title mdl-card--expand" >';
								$disp['room'][$ide].="<h4> Room:";
									$disp['room'][$ide].=" Pédiatrie<br>";
		
									$disp['room'][$ide].=$id."#";
								$disp['room'][$ide].="</h4>";
							$disp['room'][$ide].="</div>";
							$disp['room'][$ide].='<div class="mdl-card__actions mdl-card--border" onclick="changeColorID($ide)">';
								$disp['room'][$ide].='<a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">';
								$disp['room'][$ide].="	Add to Calendar";
								$disp['room'][$ide].="</a></div></th>";
								
					}
					for($i=1; $i<4;$i++){ // on veut afficher les rooms(#2) et les salles d'opérations (#1)
						$ide=$i+2;
						$id='1'.$ide;
						$disp['room'][$ide].='<th><div class="demo-card--media mdl-card mdl-shadow--2dp" id="$ide" onclick="location.href="infopage.php?type=3&number=$ide"">' ;
							$disp['room'][$ide].='<div class="mdl-card__title mdl-card--expand" >';
								$disp['room'][$ide].="<h4> Operation:";
									$disp['room'][$ide].="  Pédiatrie<br>";
		
									$disp['room'][$ide].=$id."#";
								$disp['room'][$ide].="</h4>";
							$disp['room'][$ide].="</div>";
							$disp['room'][$ide].='<div class="mdl-card__actions mdl-card--border" onclick="changeColorID($ide)">';
								$disp['room'][$ide].='<a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">';
								$disp['room'][$ide].="	Add to Calendar";
								$disp['room'][$ide].="</a></div></th>";
							}

							/****** 

					Urgences

					***/

					for($i=1; $i<5;$i++){ // on veut afficher les rooms(#2) et les salles d'opérations (#1)
						$ide=$i+5;
						$id='2'.$ide;
						$disp['room'][$ide].='<th><div class="demo-card--media mdl-card mdl-shadow--2dp" id="$ide" onclick="location.href="infopage.php?type=2&number=$ide"">';
							$disp['room'][$ide].='<div class="mdl-card__title mdl-card--expand" >';
								$disp['room'][$ide].="<h4> Room";
									$disp['room'][$ide].="  Urgences<br>";
		
									$disp['room'][$ide].=$id."#";
								$disp['room'][$ide].="</h4>";
							$disp['room'][$ide].="</div>";
							$disp['room'][$ide].='<div class="mdl-card__actions mdl-card--border" onclick="changeColorID($ide)">';
								$disp['room'][$ide].='<a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">';
								$disp['room'][$ide].="	Add to Calendar";
								$disp['room'][$ide].="</a></div></th>";

						}
					for($i=1; $i<4;$i++){ // on veut afficher les rooms(#2) et les salles d'opérations (#1)
						$ide=$i+7;
						$id='1'.$ide;
						$disp['room'][$ide].='<th><div class="demo-card--media mdl-card mdl-shadow--2dp" id="$ide" onclick="location.href="infopage.php?number=$ide>';
							$disp['room'][$ide].='<div class="mdl-card__title mdl-card--expand" >';
								$disp['room'][$ide].="<h4> Opération";
									$disp['room'][$ide].="  Urgences<br>";
		
									$disp['room'][$ide].=$id."#";
								$disp['room'][$ide].="</h4>";
							$disp['room'][$ide].="</div>";
							$disp['room'][$ide].='<div class="mdl-card__actions mdl-card--border" onclick="changeColorID($ide)">';
								$disp['room'][$ide].='<a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">';
								$disp['room'][$ide].="	Add to Calendar";
								$disp['room'][$ide].="</a></div></th>";

							}


							/****** 

					Oncologie

					***/


					for($i=1;$i<5;$i++){ // on veut afficher les rooms(#2) et les salles d'opérations (#1)
						$ide=$i+9;
						$id='2'.$ide;
						$disp['room'][$ide].='<th><div class="demo-card--media mdl-card mdl-shadow--2dp" id="$ide" onclick="location.href="infopage.php?type=2&number=$ide"">';
							$disp['room'][$ide].='<div class="mdl-card__title mdl-card--expand" >';
								$disp['room'][$ide].="<h4> Room:";
									$disp['room'][$ide].=" Oncologie<br>";
		
									$disp['room'][$ide].=$id."#";
								$disp['room'][$ide].="</h4>";
							$disp['room'][$ide].="</div>";
							$disp['room'][$ide].='<div class="mdl-card__actions mdl-card--border" onclick="changeColorID($ide)">';
								$disp['room'][$ide].='<a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">';
								$disp['room'][$ide].="	Add to Calendar";
								$disp['room'][$ide].="</a></div></th>";
								
					}
					for($i=1; $i<4;$i++){ // on veut afficher les rooms(#2) et les salles d'opérations (#1)
						$ide=$i+10;
						$id='1'.$ide;
						$disp['room'][$ide].='<th><div class="demo-card--media mdl-card mdl-shadow--2dp" id="$ide" onclick="location.href="infopage.php?type=3&number=$ide"">' ;
							$disp['room'][$ide].='<div class="mdl-card__title mdl-card--expand" >';
								$disp['room'][$ide].="<h4> Operation:";
									$disp['room'][$ide].="  Oncologie<br>";
		
									$disp['room'][$ide].=$id."#";
								$disp['room'][$ide].="</h4>";
							$disp['room'][$ide].="</div>";
							$disp['room'][$ide].='<div class="mdl-card__actions mdl-card--border" onclick="changeColorID($ide)">';
								$disp['room'][$ide].='<a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">';
								$disp['room'][$ide].="	Add to Calendar";
								$disp['room'][$ide].="</a></div></th>";
							}


							/****** 

					Chirurgie

					***/

					for($i=1; $i<5;$i++){ // on veut afficher les rooms(#2) et les salles d'opérations (#1)
						$ide=$i+13;
						$id='2'.$ide;
						$disp['room'][$ide].='<th><div class="demo-card--media mdl-card mdl-shadow--2dp" id="$ide" onclick="location.href="infopage.php?type=2&number=$ide"">';
							$disp['room'][$ide].='<div class="mdl-card__title mdl-card--expand" >';
								$disp['room'][$ide].="<h4> Room";
									$disp['room'][$ide].="  Chirurgie<br>";
		
									$disp['room'][$ide].=$id."#";
								$disp['room'][$ide].="</h4>";
							$disp['room'][$ide].="</div>";
							$disp['room'][$ide].='<div class="mdl-card__actions mdl-card--border" onclick="changeColorID($ide)">';
								$disp['room'][$ide].='<a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">';
								$disp['room'][$ide].="	Add to Calendar";
								$disp['room'][$ide].="</a></div></th>";

						}
					for($i=1; $i<4;$i++){ // on veut afficher les rooms(#2) et les salles d'opérations (#1)
						$ide=$i+13;
						$id='1'.$ide;
						$disp['room'][$ide].='<th><div class="demo-card--media mdl-card mdl-shadow--2dp" id="$ide" onclick="location.href="infopage.php?number=$ide>';
							$disp['room'][$ide].='<div class="mdl-card__title mdl-card--expand" >';
								$disp['room'][$ide].="<h4> Opération";
									$disp['room'][$ide].="  Chirurgie<br>";
		
									$disp['room'][$ide].=$id."#";
								$disp['room'][$ide].="</h4>";
							$disp['room'][$ide].="</div>";
							$disp['room'][$ide].='<div class="mdl-card__actions mdl-card--border" onclick="changeColorID($ide)">';
								$disp['room'][$ide].='<a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">';
								$disp['room'][$ide].="	Add to Calendar";
								$disp['room'][$ide].="</a></div></th>";

							}

							/****** 

					Obstétrique

					***/

					for($i=1; $i<5;$i++){ // on veut afficher les rooms(#2) et les salles d'opérations (#1)
						$ide=$i+17;
						$id='2'.$ide;
						$disp['room'][$ide].='<th><div class="demo-card--media mdl-card mdl-shadow--2dp" id="$ide" onclick="location.href="infopage.php?type=2&number=$ide"">';
							$disp['room'][$ide].='<div class="mdl-card__title mdl-card--expand" >';
								$disp['room'][$ide].="<h4> Room";
									$disp['room'][$ide].="  Obstétrique<br>";
		
									$disp['room'][$ide].=$id."#";
								$disp['room'][$ide].="</h4>";
							$disp['room'][$ide].="</div>";
							$disp['room'][$ide].='<div class="mdl-card__actions mdl-card--border" onclick="changeColorID($ide)">';
								$disp['room'][$ide].='<a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">';
								$disp['room'][$ide].="	Add to Calendar";
								$disp['room'][$ide].="</a></div></th>";

						}
					for($i=1; $i<4;$i++){ // on veut afficher les rooms(#2) et les salles d'opérations (#1)
						$ide=$i+16;
						$id='1'.$ide;
						$disp['room'][$ide].='<th><div class="demo-card--media mdl-card mdl-shadow--2dp" id="$ide" onclick="location.href="infopage.php?number=$ide>';
							$disp['room'][$ide].='<div class="mdl-card__title mdl-card--expand" >';
								$disp['room'][$ide].="<h4> Opération";
									$disp['room'][$ide].="  Obstétrique<br>";
		
									$disp['room'][$ide].=$id."#";
								$disp['room'][$ide].="</h4>";
							$disp['room'][$ide].="</div>";
							$disp['room'][$ide].='<div class="mdl-card__actions mdl-card--border" onclick="changeColorID($ide)">';
								$disp['room'][$ide].='<a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">';
								$disp['room'][$ide].="	Add to Calendar";
								$disp['room'][$ide].="</a></div></th>";

							}

							/****

						Les autrees matériels   3  --  Scanner    4 -- Radio

							*///
						for($i=1; $i<6;$i++){ // on veut afficher les rooms(#2) et les salles d'opérations (#1)
							if ($i ==1) $departement= "Pédiatrie";
       						if ($i ==2) $departement= "Urgences";
       						if ($i ==3) $departement= "Oncologie";
       						if ($i ==4) $departement= "Chirurgie";
       						if ($i ==5) $departement= "Obstétrique";

						$ide=22+$i;
						$id='3'.$ide;
						$disp['mat'][$ide].='<th><div class="demo-card--media mdl-card mdl-shadow--2dp" id="$ide" onclick="location.href="infopage.php?type=2&number=$ide"">';
							$disp['mat'][$ide].='<div class="mdl-card__title mdl-card--expand" >';
								$disp['mat'][$ide].="<h4> Scanner";
									$disp['mat'][$ide].=$departement."  <br>";
		
									$disp['mat'][$ide].=$id."#";
								$disp['mat'][$ide].="</h4>";
							$disp['mat'][$ide].="</div>";
							$disp['mat'][$ide].='<div class="mdl-card__actions mdl-card--border" onclick="changeColorID($ide)">';
								$disp['mat'][$ide].='<a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">';
								$disp['mat'][$ide].="	Add to Calendar";
								$disp['mat'][$ide].="</a></div></th></tr>";

							for($i=1; $i<3;$i++){ // on veut afficher les rooms(#2) et les salles d'opérations (#1)
							if ($i ==1) $departement= "Pédiatrie";
       						if ($i ==2) $departement= "Urgences";
						$ide=5+$i;
						$id='4'.$ide;
						$disp['mat'][$ide].='<th><div class="demo-card--media mdl-card mdl-shadow--2dp" id="$ide" onclick="location.href="infopage.php?type=2&number=$ide"">';
							$disp['mat'][$ide].='<div class="mdl-card__title mdl-card--expand" >';
								$disp['mat'][$ide].="<h4> Radio";
									$disp['mat'][$ide].=$departement."  <br>";
		
									$disp['mat'][$ide].=$id."#";
								$disp['mat'][$ide].="</h4>";
							$disp['mat'][$ide].="</div>";
							$disp['mat'][$ide].='<div class="mdl-card__actions mdl-card--border" onclick="changeColorID($ide)">';
								$disp['mat'][$ide].='<a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">';
								$disp['mat'][$ide].="	Add to Calendar";
								$disp['mat'][$ide].="</a></div></th></tr>";
								}

							for($i=3; $i<6;$i++){ // on veut afficher les rooms(#2) et les salles d'opérations (#1)
								if ($i ==3) $departement= "Oncologie";
       							if ($i ==4) $departement= "Chirurgie";
       							if ($i ==5) $departement= "Obstétrique";
						$ide=22+$i;
						$id='4'.$ide;
						$disp['mat'][$ide].='<th><div class="demo-card--media mdl-card mdl-shadow--2dp" id="$ide" onclick="location.href="infopage.php?type=2&number=$ide"">';
							$disp['mat'][$ide].='<div class="mdl-card__title mdl-card--expand" >';
								$disp['mat'][$ide].="<h4> Radio";
									$disp['mat'][$ide].=$departement."  <br>";
		
									$disp['mat'][$ide].=$id."#";
								$disp['mat'][$ide].="</h4>";
							$disp['mat'][$ide].="</div>";
							$disp['mat'][$ide].='<div class="mdl-card__actions mdl-card--border" onclick="changeColorID($ide)">';
								$disp['mat'][$ide].='<a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">';
								$disp['mat'][$ide].="	Add to Calendar";
								$disp['mat'][$ide].="</a></div></th></tr>";
								}	
						$inf=$ipat+$inur+$idoc+45;
					for($i=1;$i<$inf;$i++){

						if($disp['doc'][$i]!=null)echo $disp['doc'][$i];
						if($disp['pat'][$i]!=null)echo $disp['pat'][$i];
						if($disp['nur'][$i]!=null)echo $disp['nur'][$i];
						if($disp['room'][$i]!=null)echo $disp['room'][$i];
						if($disp['mat'][$i]!=null)echo $disp['mat'][$i];						

					}
					

				?>
			


			</table>

		</div>

	<!-- Colored FAB button with ripple SI c'est autre -->
<!----
					ICI CODER LE JS QUI REGARDE SI ON A BIEN SELECTIONE SUFFISAMENT DE CARTES
					SINON PREVENIR L'UTILISATEUR
					DONNER LES INFOS DU NOMBRE DE CARTE DANS CHAQUES LISTES POUR QU'IL PUISSE LES PARCOURIRS
				
				---->

	<button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored buttonplus" onclick="newCalendar(<?php echo Database::getAllCount(); ?>)">
  <i class="material-icons">add</i>
	</button>

		

	<footer>
			<div >
				<div >
					&nbsp;&nbsp; © ECE paris &nbsp; &nbsp;  
					 <a href="mailto:jkobject@gmail.com"> mail me </a>
					 
				<div class="rightfoot1">
								<a href="about.php">Newsletter </a>
									 <a class="rightfoot2">  &nbsp; &nbsp;  &nbsp; Designed by &nbsp; &nbsp; <strong><span>jkobject and Kwita</span></strong></a>
				</div> 
			</div>
		
		</footer>
	</body>
</html>
