<?php
     
    
class Database {
    
    public static $bdd = null; // on agit comme cela car on est en objet. je met des static comme ça pas beosin de l'instancier. 
                                // se rappeler du java pour coder 

                // self == this. En objet penser a le mettre pour dire que l'on parle de l'instance sur laquelle on se trouve. ( ici ,la seule )
   public static function open(){ // fonction a appeler tout le temp qui ouvre la base de données
        
        if(!isset(self::$bdd)){
        try {

            //self::$bdd = new PDO('mysql:host=localhost; dbname=sharpinbdd;charset=utf8', 'root', 'root'); //config mac
			self::$bdd = new PDO('mysql:host=localhost; dbname=healthroom;charset=utf8', 'root', '');//config windows
            //self::$bdd = new PDO('mysql:host=localhost; dbname=healthroom; charset=utf8', 'root', '',array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
           
            
        }
        catch (Exception $e){
            
            echo "Erreur";
        }
        }
    }
    public static function getAllCount()  // donne les infos de nombre utilisable en string
    {
        
        self::open();
    
        $sql="SELECT COUNT('number') FROM `user` WHERE  `grade` = 1"; 
        // texte de la requete ( toujours les tester sur phpmyadmin avant d'en faire de nouveaux)
        $res = self::$bdd->query($sql); // fait la requete
        $ret =$res->fetch();

        $ret .=",";
        $sql="SELECT COUNT('number') FROM `user` WHERE  `grade` = 2"; 
        // texte de la requete ( toujours les tester sur phpmyadmin avant d'en faire de nouveaux)
    
        $res = self::$bdd->query($sql); // fait la requete
        $ret .=$res->fetch();
        $ret .=",";
       $sql="SELECT COUNT('number') FROM `user` WHERE  `grade` = 3"; 
        // texte de la requete ( toujours les tester sur phpmyadmin avant d'en faire de nouveaux)
        $res = self::$bdd->query($sql); // fait la requete
        $ret .=$res->fetch();
        $ret .=",";
        $ret .="35";
        $ret .=",";
        $ret .="10";
        return $ret; // fetch donne la premiere ligne des resultat et les classes bien comme il faut// ici on l'utilise car une seule ligne 
        
    }

    public static function searchUser($number,$password)  // fait une recherche au moyen de la fonction REGEXP ( très sympa) comme les 3 autres fonctions
                                        /// A appelé avec ce que l'on récupère depuis la zone de recherche. Faire Attention A CE QUE L'ON RECUPERE !!!
    {
        
        self::open();
    
        $sql="SELECT * FROM `user` WHERE  `num` = '{$number}' AND `password` = '{$password}'"; 
        // texte de la requete ( toujours les tester sur phpmyadmin avant d'en faire de nouveaux)
        $res = self::$bdd->query($sql); // fait la requete

        return $res->fetch(); // fetch donne la premiere ligne des resultat et les classes bien comme il faut// ici on l'utilise car une seule ligne 
        
    }
    public static function searchUserAll()  // fait une recherche au moyen de la fonction REGEXP ( très sympa) comme les 3 autres fonctions
                                        /// A appelé avec ce que l'on récupère depuis la zone de recherche. Faire Attention A CE QUE LON RECUPERE !!!
    {
        
        self::open();
    
        $sql="SELECT * FROM `user` WHERE  1"; 
        // texte de la requete ( toujours les tester sur phpmyadmin avant d'en faire de nouveaux)
        $res = self::$bdd->query($sql); // fait la requete

        return $res->fetch(); // fetch donne la premiere ligne des resultat et les classes bien comme il faut// ici on l'utilise car une seule ligne 
        
    }

    public static function newUser ($grade,$dpt,$mdp,$nom,$pre,$num){
        
        self::open();
        $sql="INSERT INTO `user`( `name`, `lastname`, `password`, `grade`, `iddepartement`,`num`) VALUES";
        $sql.= "('{$nom}','{$pre}','{$mdp}','{$grade}','{$dpt}',{$num})";
        $res = self::$bdd->query($sql); // on creer un nouvel user
        if($res == null)  echo "Erreur"; // si ça n'a pas marcher on l'affiche
    
    }
    /****

IL PEU Y AVOIR DES ERREURE A PARTIR D'ICI A VERIFIER

    *****/

    public static function majChambre($idchambre, $iduser)   // met a jour la chambre
    {
        self::open();
        $sql="UPDATE `healthroom`.`chambre` SET ";
        $sql.="`iduser` = {$iduser} WHERE `chambre`.`id` = '$idchambre'";
        $res = self::$bdd->query($sql); 
        if($res == null)  echo "Erreur"; // si ça n'a pas marcher on l'affiche
    }



    public static function newPlanning($idpatient, $idnurses, $iddoctor, $idmateriels, $debut, $fin )   // créer un nouveau planning, debut et fin doivent être des string de dates
    {

        self::open();

        $sql="INSERT INTO `planning`( `debut`, `fin`) VALUES ('{$debut}','{$fin}'); SELECT SCOPE_IDENTITY();";
        $idplanning = self::$bdd->query($sql)->fetch();

        foreach ($idmateriels as $id){

               //ON regarde si la date est dispo 

            $sql="SELECT * FROM `materielplanning` WHERE  `idmateriel` ={$id}) ";
            $sql.=" AND ('{$debut}' > (SELECT `fin` FROM `planning` WHERE `id` = (SELECT `idplanning` FROM `materielplanning` WHERE `idmateriel`={$id} )) ";
            $sql.="OR '{$fin}' < (SELECT `debut` FROM `planning` WHERE `id` = (SELECT `idplanning` FROM `materielplanning` WHERE `idmateriel`={$id} )) ";
            $res = self::$bdd->query($sql); // on creer un nouvel user
            if($res == null){
                $sql="DELETE FROM `planning` WHERE  `id` ={$idplanning}) ";
                self::$bdd->query($sql);
                return "materiel";
            }
            $sqlmat="INSERT INTO `materielplanning`( `idmateriel`, `idplanning`) VALUES ({$id},{$idplanning})";
            self::$bdd->query($sqlmat);
            

        }
        foreach ($idnurses as $id ){

               //ON regarde si la date est dispo 

            $sql="SELECT * FROM `userplanning` WHERE  `iduser` ={$id}) ";
            $sql.=" AND ('{$debut}' > (SELECT `fin` FROM `planning` WHERE `id` = (SELECT `idplanning` FROM `userplanning` WHERE `iduser`={$id} )) ";
            $sql.="OR '{$fin}' < (SELECT `debut` FROM `planning` WHERE `id` = (SELECT `idplanning` FROM `userplanning` WHERE `iduser``={$id} )) ";
            $res = self::$bdd->query($sql); // on creer un nouvel user
            if($res == null){
                $sql="DELETE FROM `planning` WHERE  `id` ={$idplanning}) ";
                self::$bdd->query($sql);
                return "nurse";
            }
            $sqluse="INSERT INTO `userplanning`( `iduser`, `idplanning`) VALUES ({$id},{$idplanning})";
            self::$bdd->query($sqluse);

        }



            $sql="SELECT * FROM `userplanning` WHERE  `iduser` ={$idpatient}) ";
            $sql.=" AND ('{$debut}' > (SELECT `fin` FROM `planning` WHERE `id` = (SELECT `idplanning` FROM `userplanning` WHERE `iduser`={$idpatient} )) ";
            $sql.="OR '{$fin}' < (SELECT `debut` FROM `planning` WHERE `id` = (SELECT `idplanning` FROM `userplanning` WHERE `iduser`={$idpatient} )) ";
            $res = self::$bdd->query($sql); // on creer un nouvel user
            if($res == null){
                $sql="DELETE FROM `planning` WHERE  `id` ={$idplanning}) ";
                self::$bdd->query($sql);
                return "patient";
            }
            $sqluse="INSERT INTO `userplanning`( `iduser`, `idplanning`) VALUES ({$idpatient},{$idplanning})";
            self::$bdd->query($sqluse);

            $sql="SELECT * FROM `userplanning` WHERE  `iduser` ={$iddoctor}) ";
            $sql.=" AND ('{$debut}' > (SELECT `fin` FROM `planning` WHERE `id` = (SELECT `idplanning` FROM `userplanning` WHERE `iduser`={$iddoctor} )) ";
            $sql.="OR '{$fin}' < (SELECT `debut` FROM `planning` WHERE `id` = (SELECT `idplanning` FROM `userplanning` WHERE `iduser`={$iddoctor} )) ";
            $res = self::$bdd->query($sql); // on creer un nouvel user
            if($res == null){
                $sql="DELETE FROM `planning` WHERE `id` ={$idplanning}) ";
                self::$bdd->query($sql);
                return "doctor";
            }
            $sqluse="INSERT INTO `userplanning`( `iduser`, `idplanning`) VALUES ({$iddoctor},{$idplanning})";
            self::$bdd->query($sqluse);
    }


    public static function deletePlanning($idplanning)  { // créer un nouveau planning, debut et fin doivent être des string de dates
            $sql="DELETE FROM `planning` WHERE  `id` ={$idplanning}) ";
            self::$bdd->query($sql);
    }




}
?>
