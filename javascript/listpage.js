angular.module('testMod', [])
.controller 'testCtrl', ($scope) ->
  $scope.date = new Date()
.directive 'timeDatePicker', ['$filter', '$sce', ($filter, $sce) ->
  restrict: 'AE'
  replace: true
  scope:
    _modelValue: '=ngModel'
  require: 'ngModel'
  templateUrl: 'time-date.tpl'
  link: (scope, element, attrs, ngModel) ->
    scope._mode = attrs.defaultMode ? 'date'
    scope._displayMode = attrs.displayMode
    ngModel.$render = ->
      scope.date = if ngModel.$modelValue? then new Date ngModel.$modelValue else new Date()
      scope.calendar._year = scope.date.getFullYear()
      scope.calendar._month = scope.date.getMonth()
      scope.clock._minutes = scope.date.getMinutes()
    scope.save = -> scope._modelValue = scope.date
    scope.cancel = -> ngModel.$render()
  controller: ['$scope', (scope) ->
    scope.date = new Date()
    scope.display =
      title: ->
        if scope._mode is 'date' then $filter('date') scope.date, 'EEEE h:mm a'
        else $filter('date') scope.date, 'MMMM d yyyy'
      super: ->
        if scope._mode is 'date' then $filter('date') scope.date, 'MMM'
        else ''
      main: -> $sce.trustAsHtml(
        if scope._mode is 'date' then $filter('date') scope.date, 'd'
        else "#{$filter('date') scope.date, 'h:mm'}<small>#{$filter('date') scope.date, 'a'}</small>"
      )
      sub: ->
        if scope._mode is 'date' then $filter('date') scope.date, 'yyyy'
        else $filter('date') scope.date, 'HH:mm'
    scope.calendar =
      _month: 0
      _year: 0
      _months: ["January","February","March","April","May","June","July","August","September","October","November","December"]
      offsetMargin: -> "#{new Date(@_year, @_month).getDay() * 3.6}rem"
      isVisible: (d) -> new Date(@_year, @_month, d).getMonth() is @_month
      class: (d) ->
        if new Date(@_year, @_month, d).getTime() is new Date(scope.date.getTime()).setHours(0,0,0,0) then "selected"
        else if new Date(@_year, @_month, d).getTime() is new Date().setHours(0,0,0,0) then "today"
        else ""
      select: (d) -> scope.date.setFullYear @_year, @_month, d
      monthChange: ->
        if not @_year? or isNaN @_year then @_year = new Date().getFullYear()
        scope.date.setFullYear @_year, @_month
        if scope.date.getMonth() isnt @_month then scope.date.setDate 0
    scope.clock =
      _minutes: 0
      _hour: ->
        _h = scope.date.getHours()
        _h = _h % 12
        return if _h is 0 then 12 else _h
      setHour: (h) ->
        if h is 12 and @isAM() then h = 0
        h += if not @isAM() then 12 else 0
        if h is 24 then h = 12
        scope.date.setHours h
      setAM: (b) -> if b and not @isAM() then scope.date.setHours(scope.date.getHours() - 12) else if not b and @isAM() then scope.date.setHours(scope.date.getHours() + 12)
      isAM: -> scope.date.getHours() < 12
    scope.$watch 'clock._minutes', (val) ->
      if val? and val isnt scope.date.getMinutes() then scope.date.setMinutes val
    scope.setNow = -> scope.date = new Date()
    scope._mode = 'date'
    scope.modeClass = ->
      if scope._displayMode? then scope._mode = scope._displayMode
      if scope._displayMode is 'time' then 'time-only'
      else if scope._displayMode is 'date' then 'date-only'
      else if scope._mode is 'date' then 'date-mode'
      else 'time-mode'
    scope.modeSwitch = -> scope._mode = scope._displayMode ? if scope._mode is 'date' then 'time' else 'date'
]]